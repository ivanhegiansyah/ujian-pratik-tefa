import "./App.css";
import { useState } from "react";

function App() {
  const [val, setVal] = useState("");
  const [capVal, setCapVal] = useState("");
  const [revVal, setRevVal] = useState("");
  const [mostFreq, setMostFreq] = useState("");
  const [wordCount, setWordCount] = useState("");
  const [charCount, setCharCount] = useState("");

  function capitalizeWord(words) {
    var separateWord = words.toLowerCase().split(" ");
    for (var i = 0; i < separateWord.length; i++) {
      separateWord[i] =
        separateWord[i].charAt(0).toUpperCase() + separateWord[i].substring(1);
    }
    return separateWord.join(" ");
  }

  function reverse(str, start, end) {
    let temp;

    while (start <= end) {
      temp = str[start];
      str[start] = str[end];
      str[end] = temp;
      start++;
      end--;
    }
  }

  function reverseWords(s) {
    s = s.split("");
    let start = 0;
    for (let end = 0; end < s.length; end++) {
      if (s[end] === " ") {
        reverse(s, start, end);
        start = end + 1;
      }
    }
    reverse(s, start, s.length - 1);
    reverse(s, 0, s.length - 1);
    return s.join("");
  }

  function freqCharacter(input) {
    const { max, ...counts } = (input || "").split("").reduce(
      (a, c) => {
        a[c] = a[c] ? a[c] + 1 : 1;
        a.max = a.max < a[c] ? a[c] : a.max;
        return a;
      },
      { max: 0 }
    );

    return Object.entries(counts).filter(([k, v]) => v === max);
  }

  const logic = () => {
    setCapVal(capitalizeWord(val));
    setRevVal(reverseWords(val));
    setMostFreq(freqCharacter(val).join(" | ").replace(/,/g, ":"));
    setWordCount(val.match(/(\w+)/g).length);
    setCharCount(val.length);
  };
  return (
    <div className="container">
      <div className="app">
        <div className="input">
          <h1>Input:</h1>
          <textarea
            cols="40"
            rows="10"
            onChange={(e) => {
              setVal(e.target.value);
            }}
            value={val}
          />
          <p></p>
          <button
            onClick={() => {
              logic();
            }}
          >
            Convert
          </button>
        </div>
        <div className="output">
          <h1>Output:</h1>
          <p>{`Capitalize version: ${capVal}`}</p>
          <p>{`Reversed version: ${revVal}`}</p>
          <p>{`Most frequent character: {${mostFreq}}`}</p>
          <p>{`Word count: ${wordCount}`}</p>
          <p>{`Character count: ${charCount}`}</p>
        </div>
      </div>
    </div>
  );
}

export default App;

// hari ini adalah ujian praktik tefa chapter 1 aaaaaaaaaaaa
