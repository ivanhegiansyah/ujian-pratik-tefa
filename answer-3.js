class World {
  constructor(num) {
    this.cities = new Araay(num);
  }

  add_city(name) {
    this.cities.push(new City(name));
  }

  random_city() {
    
  }

  total_cities() {
    return this.cities.length;
  }
}

class City {
  constructor(name) {
    if (name !== null) {
      this.name = name;
    }
    this.rand_name();
    this.citizens;
  }

  add_citizen() {
    if (this.citizens === null) {
      [new Citizen()];
    }
    return this.citizens.push(new Citizen());
  }

  rand_name() {
    let res = "";
    let c = "abcdefghijklmnopqrstuvwxyz";

    for (let i = 0; i < 5; i++) {
      res += c.charAt(Math.floor(Math.random() * c.length));
    }
    return res;
  }
}

class Citizen {
  constructor() {
    this.age = Math.floor(Math.random() * 100);
  }
}

const world = new World(50);
world.add_city("Jakarta");
console.log("Random city name: ", world.random_city().name);
// harusnya mengeluarkan value semacam "Jakarta" atau nilai random lainnya yang panjang characternya 5.
console.log(
  "Age of first citizen in another random city: ",
  world.random_city().citizens[0]
);
// harusnya mengeluarkan sebuah angka dari 0-100
console.log("# of Cities: ", world.total_cities());
// harusnya mengeluarkan angka 51
