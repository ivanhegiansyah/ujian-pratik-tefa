const values = {
  name: "John Doe",
  age: 21,
  username: "johndoe",
  password: "abc123",
  location: "Jakarta",
  time: new Date(),
};

const objToString = (obj) => {
  const newValues = Object.assign({}, obj);
  delete newValues.password;
  delete newValues.time;
  console.log(JSON.stringify(newValues));
};

objToString(values);
